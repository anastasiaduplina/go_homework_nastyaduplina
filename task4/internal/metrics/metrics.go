package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Metrics struct {
	CountSendMoney           *prometheus.Counter
	CountGetBalanceIdEquals1 *prometheus.Counter
	CountMoneyMore100        *prometheus.Counter
}

func InitMetrics() *Metrics {
	countSendMoney := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "bank", Name: "countSendMoney", Help: "count requests SendMoney",
	})
	countGetBalanceIdEquals1 := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "bank", Name: "countGetBalanceIdEquals1", Help: "count GetBalanceIdEquals1",
	})
	countMoneyMore10 := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "bank", Name: "countMoneyMore10", Help: "count requests who send more 100 money",
	})
	metrics := &Metrics{
		CountSendMoney:           &countSendMoney,
		CountGetBalanceIdEquals1: &countGetBalanceIdEquals1,
		CountMoneyMore100:        &countMoneyMore10,
	}
	return metrics
}
