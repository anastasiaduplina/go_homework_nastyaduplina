package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/juju/zaputil/zapctx"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.uber.org/zap"
	"io"
	"net/http"
	"strconv"
	"task4/bankService"
	"task4/internal/config"
	otel2 "task4/internal/http_server"
	"task4/internal/metrics"
	"task4/internal/model"
)

type Handler struct {
	cfg     *config.Config
	bank    *bankService.BankService
	logger  *zap.Logger
	metrics *metrics.Metrics
}

func NewHandlers(config *config.Config, bank *bankService.BankService, ctx context.Context) *Handler {
	logger := zapctx.Logger(ctx)
	initMetrics := metrics.InitMetrics()
	return &Handler{
		cfg:     config,
		bank:    bank,
		logger:  logger,
		metrics: initMetrics,
	}
}

func (h *Handler) SaveMoneyHandler(w http.ResponseWriter, r *http.Request) {
	newCtx, span := otel2.Tracerr.Start(r.Context(), r.URL.Path)
	defer span.End()

	h.logger.Info("Incoming request",
		zap.String("method", r.Method),
		zap.String("path", r.URL.Path))

	span.SetAttributes(
		attribute.String("http.method", r.Method),
		attribute.String("http.path", r.URL.Path),
	)

	if r.Method != http.MethodPut {
		err := errors.New("invalid method")
		h.logger.Error("Invalid method",
			zap.String("method", r.Method))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("invalid method try put").Error(), http.StatusBadRequest)
		return
	}

	var request model.SaveMoneyRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		h.logger.Error("Error with decoder", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	span.SetAttributes(
		attribute.Int("http.req.body.id", request.Id),
		attribute.Int("http.req.body.count", request.Count),
	)
	h.logger.Debug("Request body",
		zap.String("id", strconv.Itoa(request.Id)),
		zap.String("count", strconv.Itoa(request.Count)))

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			h.logger.Error("Error with closing body", zap.Error(err))

			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	if request.Count < 0 {
		err := errors.New("the count cannot be negative")
		h.logger.Error("The count cannot be negative",
			zap.String("count", strconv.Itoa(request.Count)))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("сумма не может быть отрицательна").Error(), http.StatusBadRequest)
		return
	}

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.SaveMoney(request.Id, request.Count, newCtx)
	if err != nil {
		h.logger.Error("Error with saving money", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		h.logger.Error("Error with encoding answer", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	h.logger.Info("Answer",
		zap.String("path", r.URL.Path),
		zap.String("answer", string(answer)))

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}

func (h *Handler) SendMonetHandler(w http.ResponseWriter, r *http.Request) {
	newCtx, span := otel2.Tracerr.Start(r.Context(), r.URL.Path)
	defer span.End()

	span.SetAttributes(
		attribute.String("http.method", r.Method),
		attribute.String("http.path", r.URL.Path),
	)

	h.logger.Info("Incoming request",
		zap.String("method", r.Method),
		zap.String("path", r.URL.Path))

	if r.Method != http.MethodPut {
		err := errors.New("invalid method")
		h.logger.Error("Invalid method",
			zap.String("method", r.Method))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("invalid method try put").Error(), http.StatusBadRequest)
		return
	}

	var request model.SendMoneyRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		h.logger.Error("Error with decoder", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	span.SetAttributes(
		attribute.Int("http.req.body.id_from", request.IdFrom),
		attribute.Int("http.req.body.id_to", request.IdTo),
		attribute.Int("http.req.body.count", request.Count),
	)

	h.logger.Debug("Request body",
		zap.String("id_from", strconv.Itoa(request.IdFrom)),
		zap.String("id_to", strconv.Itoa(request.IdTo)),
		zap.String("count", strconv.Itoa(request.Count)))

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			h.logger.Error("Error with closing body", zap.Error(err))

			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	if request.Count < 0 {
		err := errors.New("the count cannot be negative")
		h.logger.Error("The count cannot be negative",
			zap.String("count", strconv.Itoa(request.Count)))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("сумма не может быть отрицательна").Error(), http.StatusBadRequest)
		return
	}
	if request.IdFrom == request.IdTo {
		err := errors.New("id_from and id_to can`t be equal")

		h.logger.Error("id_from and id_to can`t be equal",
			zap.String("id_from", strconv.Itoa(request.IdFrom)),
			zap.String("id_to", strconv.Itoa(request.IdTo)))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("нельзя переводить самому себе").Error(), http.StatusBadRequest)
		return
	}

	if request.Count >= 100 {
		(*h.metrics.CountMoneyMore100).Inc()
	}

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.SendMoney(request.IdFrom, request.IdTo, request.Count, newCtx)
	if err != nil {
		h.logger.Error("Error with sending money", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		h.logger.Error("Error with encoding answer", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	(*h.metrics.CountSendMoney).Inc()

	h.logger.Info("Answer",
		zap.String("path", r.URL.Path),
		zap.String("answer", string(answer)))

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}

func (h *Handler) GetBalanceHandler(w http.ResponseWriter, r *http.Request) {
	newCtx, span := otel2.Tracerr.Start(r.Context(), r.URL.Path)
	defer span.End()

	span.SetAttributes(
		attribute.String("http.method", r.Method),
		attribute.String("http.path", r.URL.Path),
	)

	h.logger.Info("Incoming request",
		zap.String("method", r.Method),
		zap.String("path", r.URL.Path))

	if r.Method != http.MethodGet {
		err := errors.New("invalid method")

		h.logger.Error("Invalid method",
			zap.String("method", r.Method))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, errors.New("invalid method try get").Error(), http.StatusBadRequest)
		return
	}

	var request model.GetBalanceRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		h.logger.Error("Error with decoder", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	span.SetAttributes(
		attribute.Int("http.req.body.id", request.Id),
	)

	h.logger.Debug("Request body",
		zap.String("id", strconv.Itoa(request.Id)))

	if request.Id == 1 {
		(*h.metrics.CountGetBalanceIdEquals1).Inc()
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			h.logger.Error("Error with closing body", zap.Error(err))

			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.GetBalance(request.Id, newCtx)
	if err != nil {
		h.logger.Error("Error with getting balance", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		h.logger.Error("Error with encoding answer", zap.Error(err))

		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	h.logger.Info("Answer",
		zap.String("path", r.URL.Path),
		zap.String("answer", string(answer)))

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}
