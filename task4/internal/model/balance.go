package model

type Balance struct {
	Id    int `json:"id"`
	Money int `json:"money"`
}

type SaveMoneyRequest struct {
	Id    int `json:"id"`
	Count int `json:"count"`
}

type SendMoneyRequest struct {
	IdFrom int `json:"id_from"`
	IdTo   int `json:"id_to"`
	Count  int `json:"count"`
}

type GetBalanceRequest struct {
	Id int `json:"id"`
}

type Response struct {
	Balance Balance `json:"balance"`
	Error   error   `json:"error"`
}
