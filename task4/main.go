package main

import (
	"context"
	_ "embed"
	"flag"
	"fmt"
	"github.com/juju/zaputil/zapctx"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"task4/app"
	"task4/internal/config"
	otel2 "task4/internal/http_server"
	l "task4/internal/logger"
	"time"
)

func main() {
	shutdown := otel2.InitProvider()
	defer shutdown()

	logger, err := l.GetLogger(true)
	if err != nil {
		log.Fatal(err)
	}

	var cfgPath string
	flag.StringVar(&cfgPath, "cfg", ".config.yaml", "set cfg path")
	flag.Parse()

	cfg, err := config.NewConfig(cfgPath)
	if err != nil {
		logger.Error("Error with config", zap.Error(err))
		fmt.Println(fmt.Errorf("fatal: init config %w", err))
		os.Exit(1)
	}

	logger.Info("Config is ready")
	ctxLogger := zapctx.WithLogger(context.Background(), logger)

	a := app.NewApp(cfg, ctxLogger)

	http.Handle("/metrics", promhttp.Handler())

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	defer func() {
		v := recover()

		if v != nil {
			logger.Error("Some unrecoverable error", zap.Error(err))
			ctx, _ := context.WithTimeout(ctx, 3*time.Second)
			a.Stop(ctx, ctxLogger)
			fmt.Println(v)
			os.Exit(1)
		}
	}()

	a.Run(ctxLogger)
	<-ctx.Done()
	ctx, _ = context.WithTimeout(ctx, 3*time.Second)
	a.Stop(ctx, ctxLogger)
}
