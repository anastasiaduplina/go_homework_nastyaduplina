package app

import (
	"context"
	"fmt"
	"github.com/juju/zaputil/zapctx"
	"net/http"
	"task4/bankService"
	"task4/internal/config"
	"task4/internal/http_server/handlers"
	"task4/storage"
)

type App struct {
	cfg         *config.Config
	server      *http.Server
	storage     *storage.Storage
	bankService *bankService.BankService
}

func NewApp(cfg *config.Config, ctx context.Context) *App {
	address := fmt.Sprintf(":%d", cfg.Http.Port)

	var store storage.Storage = storage.NewStorage()
	var bank bankService.BankService = bankService.NewBankService(&store)

	a := &App{
		cfg: cfg,
		server: &http.Server{
			Addr:    address,
			Handler: initApi(cfg, &bank, ctx),
		},
		bankService: &bank,
		storage:     &store,
	}

	return a
}

func (a *App) Run(ctx context.Context) {
	logger := zapctx.Logger(ctx)
	go func() {
		logger.Info("Server start 8080")
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}

	}()
	go func() {
		logger.Info("Server start 9000")
		err := http.ListenAndServe(":9000", nil)
		logger.Error("Error with app3")
		if err != nil {
			fmt.Println(err)
		}

	}()
}

func initApi(cfg *config.Config, bank *bankService.BankService, ctx context.Context) http.Handler {
	mux := http.NewServeMux()
	newHandlers := handlers.NewHandlers(cfg, bank, ctx)

	mux.HandleFunc("/saveMoney", newHandlers.SaveMoneyHandler)
	mux.HandleFunc("/sendMoney", newHandlers.SendMonetHandler)
	mux.HandleFunc("/balance", newHandlers.GetBalanceHandler)

	return mux
}

func (a *App) Stop(ctx context.Context, ctxLog context.Context) {
	logger := zapctx.Logger(ctxLog)
	logger.Info("Server stop")
	fmt.Println(a.server.Shutdown(ctx))

}
