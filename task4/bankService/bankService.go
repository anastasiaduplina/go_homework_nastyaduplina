package bankService

import (
	"context"
	"errors"
	otel2 "task4/internal/http_server"
	"task4/internal/model"
	"task4/storage"
)

type BankService interface {
	SaveMoney(id int, count int, ctx context.Context) (model.Balance, error)
	SendMoney(idFrom int, idTo int, count int, ctx context.Context) (model.Balance, error)
	GetBalance(id int, ctx context.Context) (model.Balance, error)
}
type Bank struct {
	storage *storage.Storage
}

func NewBankService(storage2s *storage.Storage) *Bank {
	return &Bank{
		storage: storage2s,
	}
}
func (bank *Bank) SaveMoney(id int, count int, ctx context.Context) (model.Balance, error) {
	_, span := otel2.Tracerr.Start(ctx, "saveMoney")
	defer span.End()

	if count < 0 {
		return model.Balance{}, errors.New("сумма не может быть отрицательна")
	}

	store := *bank.storage

	balance, err := store.Find(id)
	if err != nil {
		return balance, err
	}

	balance.Money += count
	_, err = store.Save(balance)
	if err != nil {
		return balance, err
	}

	return balance, nil
}
func (bank *Bank) SendMoney(idFrom int, idTo int, count int, ctx context.Context) (model.Balance, error) {
	_, span := otel2.Tracerr.Start(ctx, "sendMoney")
	defer span.End()

	if count < 0 {
		return model.Balance{}, errors.New("сумма не может быть отрицательна")
	}
	if idFrom == idTo {
		return model.Balance{}, errors.New("нельзя переводить самому себе")
	}

	store := *bank.storage

	balanceFrom, err := store.Find(idFrom)
	if err != nil {
		return model.Balance{}, err
	}

	balanceTo, err := store.Find(idTo)
	if err != nil {
		return model.Balance{}, err
	}

	if balanceFrom.Money >= count {
		balanceFrom.Money -= count
		balanceTo.Money += count

		_, err := store.Save(balanceFrom)
		if err != nil {
			return model.Balance{}, err
		}

		_, err = store.Save(balanceTo)
		if err != nil {
			return model.Balance{}, err
		}
	} else {
		return model.Balance{}, errors.New("недостаточно средств для перевода")
	}
	return balanceFrom, nil
}
func (bank *Bank) GetBalance(id int, ctx context.Context) (model.Balance, error) {
	_, span := otel2.Tracerr.Start(ctx, "getBalance")
	defer span.End()

	store := *bank.storage
	balanceFrom, err := store.Find(id)
	if err != nil {
		return balanceFrom, err
	}
	return balanceFrom, nil
}
