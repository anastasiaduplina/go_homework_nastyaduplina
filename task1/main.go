package main

import (
	"fmt"
	"hash/fnv"
)

type Book struct {
	Name   string
	Author string
	Pages  int
}

type Storage interface {
	SaveBook(id int, book *Book)
	GetSize() int
	GetBook(id int) *Book
}

type StorageMap struct {
	Books map[int]*Book
	Size  int
}

func (storage *StorageMap) GetBook(id int) *Book {
	return storage.Books[id]
}
func (storage *StorageMap) GetSize() int {
	return storage.Size
}
func (storage *StorageMap) SaveBook(id int, book *Book) {
	storage.Books[id] = book
	storage.Size += 1
}

type StorageSlice struct {
	Books []*Book
	Size  int
}

func (storage *StorageSlice) GetBook(id int) *Book {
	id -= 1
	return storage.Books[id]
}

func (storage *StorageSlice) GetSize() int {
	return storage.Size
}
func (storage *StorageSlice) SaveBook(id int, book *Book) {
	storage.Books = append(storage.Books, book)
	storage.Size += 1
}

type Library struct {
	Storage  Storage
	CreateId func(lib *Library, name string) int
	IdBooks  map[string]int
}

func (lib *Library) GetBookByName(name string) *Book {
	var id = lib.IdBooks[name]
	return lib.Storage.GetBook(id)
}
func (lib *Library) saveBookInLibrary(book *Book) {
	var id = lib.CreateId(lib, book.Name)
	lib.Storage.SaveBook(id, book)
	lib.IdBooks[book.Name] = id
}

func IdForMap(lib *Library, name string) int {
	hash := fnv.New32a()
	hash.Write([]byte(name))
	return int(hash.Sum32())
}
func IdForSlice(lib *Library, name string) int {
	return lib.Storage.GetSize() + 1
}

func main() {
	var storageMap = StorageMap{
		Books: make(map[int]*Book),
	}
	var storageSlice = StorageSlice{
		Books: make([]*Book, 0),
	}
	var library Library
	library = Library{
		Storage:  &storageMap,
		CreateId: IdForMap,
		IdBooks:  make(map[string]int),
	}

	library.saveBookInLibrary(&Book{Name: "book1", Author: "author1", Pages: 100})
	library.saveBookInLibrary(&Book{Name: "book2", Author: "author2", Pages: 200})
	library.saveBookInLibrary(&Book{Name: "book3", Author: "author3", Pages: 5})
	library.saveBookInLibrary(&Book{Name: "book4", Author: "author4", Pages: 33})
	library.saveBookInLibrary(&Book{Name: "book5", Author: "author5", Pages: 6534})

	fmt.Print(library.Storage.GetSize(), "\n")
	var book2 = library.GetBookByName("book2")
	fmt.Print(book2, "\n")
	var book3 = library.GetBookByName("book3")
	fmt.Print(book3, "\n")

	library.Storage = &storageSlice
	library.CreateId = IdForSlice

	library.saveBookInLibrary(&Book{Name: "book6", Author: "author6", Pages: 3776})
	library.saveBookInLibrary(&Book{Name: "book7", Author: "author7", Pages: 23})
	library.saveBookInLibrary(&Book{Name: "book8", Author: "author8", Pages: 233})
	library.saveBookInLibrary(&Book{Name: "book9", Author: "author9", Pages: 755})
	library.saveBookInLibrary(&Book{Name: "book10", Author: "author10", Pages: 5})

	fmt.Print(library.Storage.GetSize(), "\n")
	var book7 = library.GetBookByName("book7")
	fmt.Print(book7, "\n")
	var book10 = library.GetBookByName("book10")
	fmt.Print(book10)
}
