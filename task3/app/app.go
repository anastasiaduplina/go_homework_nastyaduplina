package app

import (
	"context"
	"fmt"
	"net/http"
	"task3/bankService"
	"task3/internal/config"
	"task3/internal/http_server/handlers"
	"task3/storage"
)

type App struct {
	cfg         *config.Config
	server      *http.Server
	storage     *storage.Storage
	bankService *bankService.BankService
}

func NewApp(cfg *config.Config) *App {

	address := fmt.Sprintf(":%d", cfg.Http.Port)

	var store storage.Storage = storage.NewStorage()
	var bank bankService.BankService = bankService.NewBankService(&store)

	a := &App{
		cfg: cfg,
		server: &http.Server{
			Addr:    address,
			Handler: initApi(cfg, &bank),
		},
		bankService: &bank,
		storage:     &store,
	}

	return a
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func initApi(cfg *config.Config, bank *bankService.BankService) http.Handler {
	mux := http.NewServeMux()
	newHandlers := handlers.NewHandlers(cfg, bank)

	mux.HandleFunc("/saveMoney", newHandlers.SaveMoneyHandler)
	mux.HandleFunc("/sendMoney", newHandlers.SendMonetHandler)
	mux.HandleFunc("/balance", newHandlers.GetBalanceHandler)

	return mux
}

func (a *App) Stop(ctx context.Context) {
	fmt.Println(a.server.Shutdown(ctx))
}
