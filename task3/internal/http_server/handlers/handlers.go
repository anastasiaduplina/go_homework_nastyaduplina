package handlers

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"task3/bankService"
	"task3/internal/config"
	"task3/internal/model"
)

type Handler struct {
	cfg  *config.Config
	bank *bankService.BankService
}

func NewHandlers(config *config.Config, bank *bankService.BankService) *Handler {
	return &Handler{
		cfg:  config,
		bank: bank,
	}
}

func (h *Handler) SaveMoneyHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		http.Error(w, errors.New("invalid method try put").Error(), http.StatusBadRequest)
		return
	}

	var request model.SaveMoneyRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	if request.Count < 0 {
		http.Error(w, errors.New("сумма не может быть отрицательна").Error(), http.StatusBadRequest)
		return
	}

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.SaveMoney(request.Id, request.Count)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}

func (h *Handler) SendMonetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		http.Error(w, errors.New("invalid method try put").Error(), http.StatusBadRequest)
		return
	}

	var request model.SendMoneyRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	if request.Count < 0 {
		http.Error(w, errors.New("сумма не может быть отрицательна").Error(), http.StatusBadRequest)
		return
	}
	if request.IdFrom == request.IdTo {
		http.Error(w, errors.New("нельзя переводить самому себе").Error(), http.StatusBadRequest)
		return
	}

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.SendMoney(request.IdFrom, request.IdTo, request.Count)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}

func (h *Handler) GetBalanceHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, errors.New("invalid method try get").Error(), http.StatusBadRequest)
		return
	}

	var request model.GetBalanceRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	var bank bankService.BankService
	bank = *h.bank
	balance, err := bank.GetBalance(request.Id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	answer, err := json.Marshal(balance)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(answer)
	return
}
