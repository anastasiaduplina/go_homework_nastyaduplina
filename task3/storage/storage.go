package storage

import (
	"errors"
	"task3/internal/model"
)

type Storage interface {
	Save(balance model.Balance) (model.Balance, error)
	Find(id int) (model.Balance, error)
}
type MyStorage struct {
	Balances *[]model.Balance
}

func NewStorage() *MyStorage {
	var balances = make([]model.Balance, 0)
	balances = append(balances, model.Balance{
		Id:    0,
		Money: 100,
	}, model.Balance{
		Id:    1,
		Money: 100,
	})
	myStore := MyStorage{Balances: &balances}
	return &myStore
}
func (myStorage *MyStorage) Save(balance model.Balance) (model.Balance, error) {
	size := len(*myStorage.Balances)

	if size > balance.Id {
		(*myStorage.Balances)[balance.Id] = balance
	} else {
		balance.Id = size
		_ = append(*myStorage.Balances, balance)
	}

	return balance, nil
}
func (myStorage *MyStorage) Find(id int) (model.Balance, error) {
	var balance model.Balance
	size := len(*myStorage.Balances)

	if size > id {
		balance = (*myStorage.Balances)[id]
	} else {
		return model.Balance{}, errors.New("не существует такого баланса")
	}
	return balance, nil
}
