package bankService

import (
	"errors"
	"task3/internal/model"
	"task3/storage"
)

type BankService interface {
	SaveMoney(id int, count int) (model.Balance, error)
	SendMoney(idFrom int, idTo int, count int) (model.Balance, error)
	GetBalance(id int) (model.Balance, error)
}
type Bank struct {
	storage *storage.Storage
}

func NewBankService(storage2s *storage.Storage) *Bank {
	return &Bank{
		storage: storage2s,
	}
}
func (bank *Bank) SaveMoney(id int, count int) (model.Balance, error) {
	if count < 0 {
		return model.Balance{}, errors.New("сумма не может быть отрицательна")
	}

	store := *bank.storage

	balance, err := store.Find(id)
	if err != nil {
		return balance, err
	}

	balance.Money += count
	_, err = store.Save(balance)
	if err != nil {
		return balance, err
	}

	return balance, nil
}
func (bank *Bank) SendMoney(idFrom int, idTo int, count int) (model.Balance, error) {
	if count < 0 {
		return model.Balance{}, errors.New("сумма не может быть отрицательна")
	}
	if idFrom == idTo {
		return model.Balance{}, errors.New("нельзя переводить самому себе")
	}

	store := *bank.storage

	balanceFrom, err := store.Find(idFrom)
	if err != nil {
		return model.Balance{}, err
	}

	balanceTo, err := store.Find(idTo)
	if err != nil {
		return model.Balance{}, err
	}

	if balanceFrom.Money >= count {
		balanceFrom.Money -= count
		balanceTo.Money += count

		_, err := store.Save(balanceFrom)
		if err != nil {
			return model.Balance{}, err
		}

		_, err = store.Save(balanceTo)
		if err != nil {
			return model.Balance{}, err
		}
	} else {
		return model.Balance{}, errors.New("недостаточно средств для перевода")
	}
	return balanceFrom, nil
}
func (bank *Bank) GetBalance(id int) (model.Balance, error) {
	store := *bank.storage
	balanceFrom, err := store.Find(id)
	if err != nil {
		return balanceFrom, err
	}
	return balanceFrom, nil
}
