package main

import (
	"bytes"
	"client/model"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"time"
)

func GetVersion() (string, error) {
	res, err := http.Get("http://localhost:8080/version")
	if err != nil {
		return "", err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			return
		}
	}(res.Body)
	b, err := httputil.DumpResponse(res, true)
	if err != nil {
		log.Fatalln(err)
	}
	return string(b), nil
}

func PostDecode() (string, error) {
	var input = model.InputString{Input: "aGVsbG8gd29ybGQ="}
	marshalled, err := json.Marshal(input)
	if err != nil {
		return "", err
	}

	res, err := http.Post("http://localhost:8080/decode", "application/json", bytes.NewReader(marshalled))
	if err != nil {
		return "", err
	}

	var output model.OutputString
	err = json.NewDecoder(res.Body).Decode(&output)
	if err != nil {
		return "", err
	}

	return output.Output, nil
}

func GetRandom(ctx context.Context) (int, error) {
	ctx, _ = context.WithTimeout(ctx, 15*time.Second)
	errs := make(chan error)
	res := make(chan int)
	go func() {
		req, err := http.Get("http://localhost:8080/hard-op")
		if err != nil {
			errs <- err
		} else {
			errs <- nil
			res <- req.StatusCode
		}
	}()
	select {
	case err := <-errs:
		if err != nil {
			return 0, err
		} else {
			return <-res, nil
		}
	case <-ctx.Done():
		return 0, fmt.Errorf("timeout")
	}
}

func main() {
	res, err := GetVersion()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	res, err = PostDecode()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	ctx := context.Background()
	status, err := GetRandom(ctx)
	if err != nil {
		fmt.Println("false")
	} else {
		fmt.Println("true", status)
	}
}
