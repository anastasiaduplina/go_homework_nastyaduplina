package app

import (
	"context"
	"fmt"
	"net/http"
	"server/internal/config"
	"server/internal/http_server/handlers"
)

type App struct {
	cfg    *config.Config
	server *http.Server
}

func NewApp(cfg *config.Config) *App {

	address := fmt.Sprintf(":%d", cfg.Http.Port)

	a := &App{
		cfg: cfg,
		server: &http.Server{
			Addr:    address,
			Handler: initApi(cfg),
		},
	}

	return a
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func initApi(cfg *config.Config) http.Handler {
	mux := http.NewServeMux()
	todoHandlers := handlers.NewTodoHandlers(cfg)

	mux.HandleFunc("/version", todoHandlers.GetVersionHandler)
	mux.HandleFunc("/decode", todoHandlers.PostDecodeHandler)
	mux.HandleFunc("/hard-op", todoHandlers.LongHandler)

	return mux
}

func (a *App) Stop(ctx context.Context) {
	fmt.Println(a.server.Shutdown(ctx))
}
