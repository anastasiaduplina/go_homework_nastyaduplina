package handlers

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"math/rand"
	"net/http"
	"server/internal/config"
	"server/internal/model"
	"time"
)

type Handler struct {
	cfg *config.Config
}

func NewTodoHandlers(config *config.Config) *Handler {
	return &Handler{
		cfg: config,
	}
}

func (h *Handler) GetVersionHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(h.cfg.Version))
	return
}

func (h *Handler) PostDecodeHandler(w http.ResponseWriter, r *http.Request) {
	var inputString model.InputString
	err := json.NewDecoder(r.Body).Decode(&inputString)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(r.Body)

	rawDecodedText, err := base64.StdEncoding.DecodeString(inputString.Input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var outputString model.OutputString
	outputString.Output = string(rawDecodedText)
	answer, err := json.Marshal(outputString)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	_, _ = w.Write(answer)
	w.WriteHeader(http.StatusOK)
	return
}

func (h *Handler) LongHandler(w http.ResponseWriter, r *http.Request) {
	messages := make(chan int)
	go func() {
		var randTime = rand.Intn(10) + 10
		time.Sleep(time.Second * time.Duration(randTime))

		messages <- 1
	}()

	select {
	case <-messages:
		var ans = rand.Intn(100)
		if ans < 50 {
			w.WriteHeader(http.StatusOK)
			return
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	case <-r.Context().Done():
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
